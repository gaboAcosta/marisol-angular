/**
 * Created by gabriel.acosta on 8/30/15.
 */
'use strict';

angular.module('marisolApp')
  .config(MarisolModule);

function MarisolModule($stateProvider) {

  $stateProvider
    .state('marisol', {
      url: '/marisol',
      templateUrl: 'app/marisol/marisol.html'
    });
}
