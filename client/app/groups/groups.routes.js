/**
 * Created by gabriel.acosta on 8/30/15.
 */
'use strict';

angular
  .module('marisolApp')
  .config(GroupModule)

function GroupModule($stateProvider){
  $stateProvider.state('groups', {
    url: '/grupos',
    templateUrl: 'app/groups/groups.html'
  })
    .state('groups:students', {
      url: '/grupos/alumnos/:id',
      templateUrl: 'app/groups/students/students.html'
    })
    .state('groups:new', {
      url: '/grupos/nuevo',
      templateUrl: 'app/groups/new/new.html'
    });
}
