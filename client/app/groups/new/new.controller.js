/**
 * Created by gabriel.acosta on 8/31/15.
 */
angular.module('marisolApp')
  .controller('NewGroupCtrl', NewGroupCtrl)

function NewGroupCtrl($http, $location){
  var vm = this;
  vm.submitted = false;
  vm.group = {};

  vm.createGroup = function(form){
    vm.submitted = true;
    if(form.$valid){
      $http.post('/api/groups/', vm.group).then(function(result){
        $location.path('/grupos')
      });
    }
  }
}
