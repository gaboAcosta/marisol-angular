/**
 * Created by gabriel.acosta on 8/30/15.
 */
'use strict';

angular.module('marisolApp')
  .controller('StudentsCtrl', StudentsCtrl);

function StudentsCtrl($stateParams){
  var vm = this;

  var groups = [
    {
      id:1,
      name: 'Primero A',
      students: [
        {
          id: 1,
          name: 'Oscar'
        },
        {
          id: 2,
          name: 'Alexa'
        },
        {
          id: 3,
          name: 'Sandra'
        },
        {
          id: 4,
          name: 'Gabriel'
        },
        {
          id: 5,
          name: 'Chuchu'
        },
      ]
    },
    {
      id:2,
      name: 'Primero B',
      students: [
        {
          id: 6,
          name: 'Marisol'
        },
        {
          id: 7,
          name: 'Luis'
        },
        {
          id: 8,
          name: 'Rosa'
        },
        {
          id: 9,
          name: 'Sofia'
        },
        {
          id: 10,
          name: 'Eduardo'
        },
      ]
    },
    {
      id:3,
      name: 'Segundo A',
      students: [
        {
          id: 11,
          name: 'Raul'
        },
        {
          id: 12,
          name: 'Ericka'
        },
        {
          id: 13,
          name: 'Soraya'
        },
        {
          id: 14,
          name: 'Ramon'
        },
        {
          id: 15,
          name: 'Benjamin'
        },
      ]
    },
    {
      id:4,
      name: 'Segundo B',
      students: [
        {
          id: 16,
          name: 'Paul'
        },
        {
          id: 17,
          name: 'Benja'
        },
        {
          id: 18,
          name: 'Ana'
        },
        {
          id: 19,
          name: 'Paquita'
        },
        {
          id: 20,
          name: 'lorena'
        },
      ]
    }
  ];
  var groupId = $stateParams.id;
  angular.forEach(groups, function(group){
    if(group.id == groupId) {
      vm.group = group;
    }
  });
}
