/**
 * Created by gabriel.acosta on 8/30/15.
 */
'use strict';

angular.module('marisolApp')
  .controller('GroupsCtrl', GroupsCtrl);

function GroupsCtrl($http){
  var vm = this;
  $http.get('/api/groups/').then(function(response){
    console.log(response);
    vm.groups = response.data;
  });
}

function OldCtrl(){
  var vm = this;
  vm.groups = [
    {
      id:1,
      name: 'Primero A'
    },
    {
      id:2,
      name: 'Primero B'
    },
    {
      id:3,
      name: 'Segundo A'
    },
    {
      id:4,
      name: 'Segundo B'
    }
  ];
}
