/**
 * Created by gabriel.acosta on 8/31/15.
 */
/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var Group = require('./group.model');

exports.index = function(req, res, next) {
  Group.find(function (err, groups) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, groups);
  });
};

exports.create = function(req, res, next){
  Group.create(req.body, function(err, group) {
    if(err) { return handleError(res, err); }
    return res.json(201, group);
  });
}
